from django.shortcuts import render
from rest_framework import generics
from book.models import *
from book.serialiser import *
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView
from book.api_response import FinalResponseObj, CustomPagination
from rest_framework import status
from rest_framework.response import Response
from rest_framework import generics
from django.contrib.auth import authenticate
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from book.authentication import CustomTokenAuthentication
from django.utils import timezone


from rest_framework import mixins, status, permissions
from rest_framework.viewsets import GenericViewSet


class CustomerListAPIView(mixins.ListModelMixin, GenericViewSet):

    pagination_class = CustomPagination
    # authentication_classes = (TokenAuthentication, )
    serializer_class = CustomerSerializer


    def get_queryset(self):
        return Customer.objects.order_by('-updated_at')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        final_response_obj = FinalResponseObj(code=status.HTTP_200_OK, body={'results': serializer.data},
                                              message='Retrieved', success=True)
        return final_response_obj.get_data()



    # def get_query_set(self, request, format=None):
    #     customers = Customer.objects.all()
    #     serializer = CustomerSerializer(customers, many=True)
    #     return Response(serializer.data)

    # def post(self, request, format=None):
    #     serializer = CustomerSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         final_response_obj = FinalResponseObj(code=status.HTTP_201_CREATED, body={'results': serializer.data}, success=True)
    #     else:
    #         final_response_obj = FinalResponseObj(code=status.HTTP_400_BAD_REQUEST, body={'results': serializer.errors}, success=False, error_msg='Invalid data')
    #     return final_response_obj.get_data()



class CustomerAPIView(APIView):

    # authentication_classes = (TokenAuthentication, )

    def get(self, request, pk, format=None):
        try:
            customer = Customer.objects.get(pk=pk)
            serializer = CustomerSerializer(customer)
            final_response_obj = FinalResponseObj(code=status.HTTP_201_CREATED, body={'results': serializer.data}, success=True)
        except Customer.DoesNotExist:
            final_response_obj = FinalResponseObj(code=status.HTTP_404_NOT_FOUND, success=False, error_msg='Not found')
        return final_response_obj.get_data()

    def put(self, request, pk, format=None):
        data = request.data
        try:
            customer = Customer.objects.get(pk=pk)
            serializer = CustomerSerializer(customer, data=data)
            if serializer.is_valid():
                serializer.save()
                final_response_obj = FinalResponseObj(code=status.HTTP_201_CREATED, body={'results': serializer.data}, success=True)
            else:
                final_response_obj = FinalResponseObj(code=status.HTTP_400_BAD_REQUEST, body={'results': serializer.errors}, success=False, error_msg='Invalid data')
        except Customer.DoesNotExist:
            final_response_obj = FinalResponseObj(code=status.HTTP_404_NOT_FOUND, success=False, error_msg='Not found')
        return final_response_obj.get_data()

    def delete(self, request, pk, format=None):
        try:
            customer = Customer.objects.get(pk=pk)
            customer.is_active= False
            customer.delete()
            final_response_obj = FinalResponseObj(code=status.HTTP_204_NO_CONTENT, success=True)
        except Customer.DoesNotExist:
            final_response_obj = FinalResponseObj(code=status.HTTP_404_NOT_FOUND, success=False, error_msg='Not found')
        return final_response_obj.get_data()


class CustomerLoginAPIView(APIView):

    # authentication_classes = (TokenAuthentication, )

    def post(self, request,  format=None):
        user = authenticate(username=request.data['username'], password=request.data['password'])
        if not user:
            response = {"error" : "invalid user"}
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        device_info = request.data['device_info']
        token, created =  CustomToken.objects.get_or_create(user=user, device_info=device_info)
        if not created:
            token.created = timezone.now()
            token.save()
        response = {'token' : token.key}
        return Response(response, status=status.HTTP_201_CREATED)


class CustomerDetailsAPIView(APIView):

    authentication_classes = (CustomTokenAuthentication, )

    def get(self, request,  format=None):
        response = {'name' : request.user.username}
        return Response(response, status=status.HTTP_201_CREATED)