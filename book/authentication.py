from django.utils.timezone import now
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication, get_authorization_header

from .models import CustomToken

class CustomTokenAuthentication(BaseAuthentication):

    model = CustomToken
    keyword = 'Token'

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            raise exceptions.AuthenticationFailed('Invalid token.')

        if len(auth) == 1:
            msg = 'Invalid token header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid token header. Token string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = 'Invalid token header. Token string should not contain invalid characters.'
            raise exceptions.AuthenticationFailed(msg)
        user, user_token = self.authenticate_credentials(token)
        # if user_token.session_data:
        #     for attr, value in user_token.session_data.items():
        #         setattr(request, attr, value)
        return user, user_token

    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.select_related('user').get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token.')

        # if token.expire_date is not None and token.expire_date < now():
        #     raise exceptions.AuthenticationFailed('Token Expired')

        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User inactive.')

        return token.user, token

    def authenticate_header(self, request):
        return self.keyword

