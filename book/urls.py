from django.conf.urls import url
from book.apis import *
from book.views import *
# apis
urlpatterns = [
                url(r'^api/customers$', CustomerListAPIView.as_view({'get': 'list'})),
                url(r'^api/customer/(?P<pk>[0-9]+)$', CustomerAPIView.as_view()),
                url(r'^api/login$', CustomerLoginAPIView.as_view()),
                url(r'^api/get-customer-name$', CustomerDetailsAPIView.as_view()),
                ]

# django web views
urlpatterns += [
                url(r'^$', CustomerListView.as_view(), name="home"),
                url(r'^login$', LoginView.as_view(), name="login"),
    			url(r'^create/$',  CustomerCreateView.as_view(), name="create"),
    			url(r'^detail/(?P<pk>\d+)/$',  CustomerDetailView.as_view(), name="details"),
    			url(r'^update/(?P<pk>\d+)/$',  CustomerUpdateView.as_view(), name="update"),
    			url(r'^delete/(?P<pk>\d+)/$',  CustomerDeleteView.as_view(), name="delete"),
                ]