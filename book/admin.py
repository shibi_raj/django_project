from django.contrib import admin
from book.models import *
# Register your models here.
admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(MyUser)
admin.site.register(CustomToken)

# from django.apps import apps
# # get_models, get_app
# for model in apps.get_models('book'):
#     admin.site.register(model)