from book.models import *
from book.forms import *
from django.views.generic import (ListView, CreateView, UpdateView, DeleteView, DetailView, View)
# from dekkho.mixins.views import StudioUserRequiredMixin
# from rest_framework.authentication import TokenAuthentication
from django.contrib.auth.decorators import login_required, permission_required

from django.shortcuts import render
from django.views.generic import View


# class CustomerListView(View):
#     def get(self,request):
#         return render(request,'home.html', )


# class CustomerListView(StudioUserRequiredMixin, ListView):
# @method_decorator(login_required, name='dispatch')
class CustomerListView(ListView):
    # permissions = {
    #     "any": ("aboutus.add_aboutus", "aboutus.change_aboutus", "aboutus.delete_aboutus")
    #     }
    model = Customer
    template_name = 'home.html'
    paginate_by = 10
    ordering = '-updated_at'


class CustomerCreateView(CreateView):
    # permissions = {
    #     "any": ("aboutus.add_aboutus", "aboutus.change_aboutus", "aboutus.delete_aboutus")
    # }
    form_class = CustomerForm
    template_name = 'create.html'
    success_url = '/'


class CustomerDetailView(DetailView):
    model = Customer
    template_name = 'details.html'


class CustomerUpdateView(UpdateView):
    # permissions = {
    #     "any": ("aboutus.add_aboutus", "aboutus.change_aboutus", "aboutus.delete_aboutus")
    # }
    form_class = CustomerForm
    model = Customer
    template_name = 'update.html'
    success_url = '/'


class CustomerDeleteView(DeleteView):
    model = Customer
    success_url = '/'
    template_name = 'customer_confirm_delete.html'

from django.shortcuts import redirect

class LoginView(View):
    def get(self,request):
        return render(request,'login.html' )

    def post(self,request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            return render(request,'login.html' , {'error_msg' : "invalid Credentials"})

