from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import LimitOffsetPagination
from collections import OrderedDict

class FinalResponseObj(object):
    
    def __init__(self, code=status.HTTP_200_OK, body={}, headers=None, success=False, message='', error_msg=''):
        self.status_code = code
        self.body = body
        self.success = success
        self.message = message
        self.header = headers
        self.error_msg = error_msg
        
    def get_data(self):
        data = {
            'body': self.body,
            'message': self.message,
            'success': self.success,
            'error_msg': self.error_msg
        }
        return Response(data, status=self.status_code, headers=self.header)
         
class CustomPagination(LimitOffsetPagination):
    default_limit = 10

    def get_paginated_response(self, data):
        body = OrderedDict([
            ('count', self.count),
            ('next', self.get_next_link() if self.get_next_link() else ''),
            ('previous', self.get_previous_link() if self.get_previous_link() else ''),
            ('results', data)
        ])
        final_response_obj = FinalResponseObj(body=body, message='Retrieved', success=True)
        return final_response_obj.get_data()