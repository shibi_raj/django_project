from django.template import Library, loader, Context

register = Library()

@register.simple_tag()
def multiply(page, index, *args, **kwargs):
    return (page - 1) * 10 +  index