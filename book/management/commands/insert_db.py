from django.core.management.base import BaseCommand, CommandError
from book.models import *
import random, uuid

class Command(BaseCommand):
    help = 'inserts dummy data'

    def add_arguments(self, parser):
        parser.add_argument('limit', nargs='+', type=int)

    def handle(self, *args, **options):
        n = 1
        for i in range(1, options['limit'][0]+1):
            c = Customer.objects.create(first_name='first_{}'.format(i), last_name='last_{}'.format(i),\
                phone_number='8089', gender=random.choice(['M', 'F']), email='user_{}@test.com'.format(i))

            if n<3:
                for j in range(n*10):
                    Order.objects.create(order_id=str(uuid.uuid1())[:8], total_price=random.randint(10,1000), customer=c)
            else:
                Order.objects.create(order_id=str(uuid.uuid1())[:8], total_price=random.randint(10,1000), customer=c)

            n+=1
        print('done')

