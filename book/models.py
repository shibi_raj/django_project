from django.db import models
# from django.contrib.gis.db.models import GeometryField
from django.conf import settings

class Customer(models.Model):
    """
    Model to store customer details
    """
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=24)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    email = models.EmailField(max_length=70)
    description = models.TextField(blank=True, null=True)
    profile_picture_url = models.TextField(blank=True, null=True)
    organization = models.CharField(max_length=100, blank=True, null=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    # location = GeometryField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return u"%s" % self.first_name

    class Meta:
        ordering = ["-updated_at"]


class Order(models.Model):
    """
    Model to store order details
    """
    order_id = models.CharField(max_length=100, unique=True)
    total_price = models.FloatField(default=0)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='orders')

    # def __str__(self):
    #     return self.order_id


from django.contrib.auth.models import AbstractUser

class MyUser(AbstractUser):
    phone_number = models.CharField(max_length=100,  unique=True)

    # USERNAME_FIELD = 'phone_number'
    # REQUIRED_FIELDS = ['phone_number']
import binascii
import os

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class CustomToken(models.Model):
    """
    The default authorization token model.
    """
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='auth_custom_token',
        on_delete=models.CASCADE, verbose_name=_("User")
    )
    device_info = models.CharField(max_length=100)
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        # Work around for a bug in Django:
        # https://code.djangoproject.com/ticket/19422
        #
        # Also see corresponding ticket:
        # https://github.com/encode/django-rest-framework/issues/705
        abstract = 'rest_framework.authtoken' not in settings.INSTALLED_APPS
        verbose_name = _("CustomToken")
        verbose_name_plural = _("Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(CustomToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key
