from rest_framework import serializers
from book.models import *

# class OrderSerializer(serializers.ModelSerializer):

#     class Meta:
#         model = Order
#         fields = '__all__'


class CustomerSerializer(serializers.ModelSerializer):

    # orders = serializers.SerializerMethodField('get_orders_custom')
    # full_name = serializers.SerializerMethodField('get_full_name_custom')
    
    # def get_orders_custom(self, obj):
    #     return OrderSerializer(obj.orders.all(), many=True).data

    # def get_full_name_custom(self, obj):
    #     return obj.first_name + ' '+ obj.last_name

    class Meta:
        model = Customer
        # fields = ('full_name', 'orders', 'first_name')
        exclude = ('created_at', 'updated_at')
        extra_kwargs = {'organization': {'read_only': True}}



# class FriendRequestSerializer(serializers.ModelSerializer):
#     first_name = serializers.SerializerMethodField('get_first_name_value')
#     image = serializers.SerializerMethodField('get_image_value')

#     def get_first_name_value(self, obj):
#         return obj.sen  der_req.first_name
#     def get_image_value(self, obj):
#         return obj.sender_req.image

#     class Meta:
#         model = FriendRequest
#         fields = ('first_name', 'image', 'status')
